WaveUp er en app som <i>vekker mobilen</i> slår skjermen på - når du <i>vinker</i> over nærhetssensor. 

Jeg har utviklet denne applikasjonen fordi jeg ville unngå å trykke på strøm knappen bare for å se på klokkeslettet - noe som jeg gjør ofte med min mobil. Det er allerede mange app-er som gjør nøyaktig dette - og enda mer. Jeg var inspirert av Gravity Screen On/Off, som er en <b>flott</b> app. Derimot, er jeg en stor fan av programvare med åpen kildekode og prøver å installere frie programmer (fri som frihet, ikke bare fri som fri øl) på mobilen min hvis mulig. Jeg kunne ikke finne en app med åpen kilkdekode som gjorde dette så jeg laget et selv. Hvis du er interessert, kan du ta en titt på kildekoden:
https://gitlab.com/juanitobananas/wave-up

Bare vink hånden din over nærhetssensoren til mobilen din for å slå skjermen på. Dette er kalt <i>wave mode</i> og kan bli slått av i instillinger for å unngå at skjermen blir slått på ved tilfeldighet. 

Den vil også slå på skjermen når du tar mobilen ut av lommen eller veske. Dette er <i> kalt lomme modus</i> og kan også bli slått av i instillinger. 

Begge av disse modusene er aktivert på forhånd.

Den låser og slår av mobilens skjerm når du dekker nærhetssensoren for en sekund (eller spesifisert tid). Dette har ikke en spesiell navn men kan likevel endres på i instillinger også. Dette er ikke slått på forhånd.

For dere som ikke har hørt om nærhetssensor før: det er en liten ting som er nær hvor du plasserer øret når du snakker på mobilen. Du kan nesten ikke se det og det er den som er ansvarlig for å fortelle mobilen din om å slå av skjermen når du er i en samtale.

<b>Avinstallere</b>

Denne appen bruker Enhets Adminstrasjons tillatelse. Derfor kan du ikke avinstallere WaveUp på 'normalt' vis. 

For å avinstallere, åpne app-en og bruk 'Avinstallere WaveUp' knappen på bunnen av menyen. 

<b>Kjent problemer</b>

Uheldigvis, lar noen smarttelefoner CPU-en på mens de lytter til nærhetssensoren. Dette er kalt <i>våkne lås</i> og forårsaker betydelig batteri tapning. Det er ikke min feil og jeg kan ikke gjøre noe for å endre det. Andre mobiler vil "gå i dvale" når skjermen er slått av og lytter fortsatt til nærhetssensoren. I dette tilfellet, er batteri tappingen er praktisk talt null.

<b>Nødvendig Android tillatelser:</b>

WAKE_LOCK for å slå skjermen på
USES_POLICY_FORCE_LOCK for å låse enheten
RECEIVE_BOOT_COMPLETED for automatisk start ved oppstart hvis valgt
READ_PHONE_STATE for å utsette WaveUp under en samtale

<b>Diverse notater</b>

Dette er første Android app-en jeg har skrevet, så pass på!

Dette er også min første lille bidrag til verden av åpen kildekode. Endelig!

Jeg ville elsket hvis du kunne gi meg tilbakemelding av noe slag eller bidra på hvilken som helst måte!

Takk for at du leser!

Åpen kildekode rocker!

<b>Oversettelser</b>

Det vil være veldig kult hvis du kunne hjelpe til å oversette WaveUp til ditt språk (selv den engelsk versjonen kan bli revidert). Dette prosjektet er tilgjengelig for oversettelse på transifex: https://www.transifex.com/jarsilio/waveup/

<b>Anerkjennelser</b>

Spesiell takk til:

Se: https://gitlab.com/juanitobananas/wave-up/#acknowledgments