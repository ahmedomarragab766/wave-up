New in 3.0.4
★ Fix small bug that kept Android's volume dialog open for very long.
★ Update Italian and Danish translations.

New in 3.0.3
★ Update Ukrainian translation. Thanks, Vlad!
★ Update Persian translation. Thanks, Moh!
★ Fix crash in some Huawei devices (excluding apps from locking might not work yet though).
★ Fix small bug in settings logic.

New in 3.0.2
★ Add crash reporting support. Reports can be sent per email. Only if the user chooses to!
★ Update some dependencies.
