New in 3.0.3
★ Update Ukrainian translation. Thanks, Vlad!
★ Update Persian translation. Thanks, Moh!
★ Fix crash in some Huawei devices (excluding apps from locking might not work yet though).
★ Fix small bug in settings logic.

New in 3.0.2
★ Add crash reporting support. Reports can be sent per email. Only if the user chooses to!
★ Update some dependencies.

New in 3.0.1-beta
★ Only Android 9: Fix 'unable to unlock using fingerprint' bug.
★ Remove 'revoke device admin' menu item.