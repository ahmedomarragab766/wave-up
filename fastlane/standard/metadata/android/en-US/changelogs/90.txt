New in 3.0.11
★ Fix small bugs while writing logs and reporting issues.

New in 3.0.10
★ Fix small "gratitude" bug.

New in 3.0.9
★ Work-around for small bug showing a "gratitude" dialog that shouldn't be shown under some circumstances.

New in 3.0.8
★ Fix crash on some devices that can't write log to cache dir.
★ Possibly fix problem while attaching logs to send with email app.
