New in 3.0.15
★ Try to start service faster to avoid Android > Oreo forcing us to stop.
  This will hopefully avoid some seldom crashes.
★ Update Russian translation.

New in 3.0.14
★ Fix bug while reading foreground app on old devices
★ Upgrade some more dependencies

New in 3.0.13
★ Update translations
★ Upgrade some dependencies

New in 3.0.12
★ Fix lock option for paid versions. There were compatibility problems when several versions where installed.
